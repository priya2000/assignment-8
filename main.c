#include <stdio.h>
struct student {
    char Name[50];
    char subject[20];
    int student;
    float marks;
} s[10];

int main() {
    int i;
    printf("Enter information of students:\n");

    // storing information
    for  (i = 0; i<5; ++i) {
        s[i].student = i + 1;
        printf("\n Student:%d,\n", s[i].student);
        printf("Enter  name: ");
        scanf("%s", s[i].Name);
        printf("Enter the subject");
        scanf("%s",s[i].subject);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("Displaying Information:\n\n");

    // displaying information
    for (i = 0; i<5; ++i) {
        printf("\n Student: %d\n", i + 1);
        printf("Name: ");
        puts(s[i].Name);
        printf("Subject: %.s", s[i].subject);
        printf("Marks: %.1f", s[i].marks);
        printf("\n");
    }
    return 0;
}
